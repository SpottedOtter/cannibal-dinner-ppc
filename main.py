import threading
import time
import logging
#from cook import Cook
#from cannibal import Cannibal

logging.basicConfig(level=logging.DEBUG,format='(%(asctime)s) (%(threadName)-9s) %(message)s',)
#logging.basicConfig(level=logging.DEBUG,format='(%(threadName)-9s) %(message)s',)

PORTIONS = 5
lock = threading.Lock()
serve_condition = threading.Condition()
cook_condition = threading.Condition()
event = threading.Event()
can_go = threading.Event()
t_end = time.time() + 2 * 60

class Cannibal(threading.Thread):

    def __init__(self, group=None, target=None, name=None,
                 args=(), kwargs=None, verbose=None):
        super(Cannibal,self).__init__()
        self.name = name
        self.served = 0
    
    def serve(self):
        global serve_condition, PORTIONS, cook_condition, event, lock, can_go
        serve_condition.acquire(blocking=True)
        logging.debug("Tentando se servir...")
        if PORTIONS > 0:
            PORTIONS -= 1
            logging.debug(self.name + " is serving..." + "  " +str(PORTIONS) + " Left.")
            serve_condition.release()
            time.sleep(1)
            return True
        else:
            event.set()
            can_go.clear()
            serve_condition.release()        
            logging.debug("There are no portions left! He wakes up the cook and goes to sleep...")
            can_go.wait()
            logging.debug("Cook ringed the bell and woke me up!")
            return False
    
    def eat(self):
        logging.debug("is eating!")
        time.sleep(4)
    
    def run(self):
        global t_end
        while True and time.time() < t_end:
            if self.serve():
                self.served += 1
                self.eat()

class Cook(threading.Thread):
    def __init__(self, group=None, target=None, name=None,
                 args=(), kwargs=None, verbose=None):
        super(Cook, self).__init__()
        self.name = name
        self.cooked = 0

    def cook_dinner(self):
        global serve_condition, cook_condition, PORTIONS, event, lock, can_go
        logging.debug("is Sleeping...")
        event.wait()
        #cook_condition.wait()
        with serve_condition:
            if PORTIONS == 0:
                logging.debug("Woke up! She is cooking...")
                #time.sleep(5)
                #PORTIONS += 5
                #event.clear() #indica se o cozinheiro pode cozinhar
                #logging.debug("cooked 5 more portions! Waking up Cannibals")
                #can_go.set() #indica se os canibais podem acordar
                #serve_condition.notify_all()
        time.sleep(5)
        with serve_condition:
            PORTIONS += 5
            event.clear()
            logging.debug("cooked 5 more portions! Waking up Cannibals")
            can_go.set() #indica se os canibais podem acordar
            serve_condition.notify_all()
        return True
    def run(self):
        global t_end
        while True and time.time() < t_end:
            if self.cook_dinner():
                self.cooked += 1
        

def main():
    global PORTIONS
    cannibals = [Cannibal(name="Cannibal-"+str(i+1)) for i in range(3)]
    cook = [Cook(name="Cook-"+str(i+1)) for i in range(1)]
    [cannibal.start() for cannibal in cannibals]
    [c.start() for c in cook]
    [cannibal.join() for cannibal in cannibals]
    logging.debug("Portions Left: {}".format(PORTIONS))
    [print(c.name + " served " + str(c.served)) for c in cannibals]

main()